package osotnikov.test.ftp;

import org.apache.sshd.common.NamedFactory;
import org.apache.sshd.common.file.FileSystemFactory;
import org.apache.sshd.common.session.Session;
import org.apache.sshd.server.Command;
import org.apache.sshd.server.SshServer;
import org.apache.sshd.server.auth.UserAuth;
import org.apache.sshd.server.auth.password.PasswordAuthenticator;
import org.apache.sshd.server.auth.password.UserAuthPasswordFactory;
import org.apache.sshd.server.keyprovider.SimpleGeneratorHostKeyProvider;
import org.apache.sshd.server.scp.ScpCommandFactory;
import org.apache.sshd.server.session.ServerSession;
import org.apache.sshd.server.subsystem.sftp.SftpSubsystemFactory;
import osotnikov.test.file.InspectableTestFileSystem;

import java.io.IOException;
import java.nio.file.FileSystem;
import java.util.ArrayList;
import java.util.List;

public class SshServerWrapper {
	
    protected SshServer sshd;
      
    private InspectableTestFileSystem fileSystem;

    private SshServerWrapper() {}
    
    public void start(int port, String username, String password, InspectableTestFileSystem fileSystem) throws IOException {
        closeFileSystem();
        setFileSystem(fileSystem);
    	
    	// Instance of Apache's MINA SshServer
        sshd = SshServer.setUpDefaultServer();
        
        // Configure port
        sshd.setPort(port);
     
        // SFTP (subsystem) command factories
        List<NamedFactory<Command>> sftpCommandFactory = new ArrayList<>();
		sftpCommandFactory.add(new SftpSubsystemFactory());
		sshd.setSubsystemFactories(sftpCommandFactory);
        // SCP command factory
        sshd.setCommandFactory( new ScpCommandFactory());
        // User authentication factories
        List<NamedFactory<UserAuth>> userAuthFactories = new ArrayList<>();
		userAuthFactories.add(new UserAuthPasswordFactory());
		sshd.setUserAuthFactories(userAuthFactories);
		// Configure server private SSL key provider ...
		sshd.setKeyPairProvider(new SimpleGeneratorHostKeyProvider());
		// Password authenticator, allow only logins with contactDetail.getSftpUsername()/contactDetail.getSftpPassword()
		sshd.setPasswordAuthenticator(new PasswordAuthenticator() {
			@Override
			public boolean authenticate(String username, String password, ServerSession session) {
				if (username.equals(username) && password.equals(password)) {
					return true;
				}
				return false;
			}
		});
		
		// Configure server's file system ...
		sshd.setFileSystemFactory(new MyFileSystemFactory());
        
        sshd.start();
    }
    
    private class MyFileSystemFactory implements FileSystemFactory {
        @Override
        public FileSystem createFileSystem(Session session) throws IOException {
            return getFileSystem();
        }
    }
    
    public InspectableTestFileSystem getFileSystem() {
		return fileSystem;
	}

	public void setFileSystem(InspectableTestFileSystem fileSystem) {
		this.fileSystem = fileSystem;
	}
    
	public void closeFileSystem() throws IOException {
		if(fileSystem != null) {
			fileSystem.reallyClose();
		}
	}
	
    public void stop() throws IOException {
    	closeFileSystem();
    	if(sshd != null) {
    		sshd.stop();
    	}
    }
}