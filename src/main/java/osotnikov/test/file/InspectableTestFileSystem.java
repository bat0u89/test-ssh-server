package osotnikov.test.file;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.UserPrincipalLookupService;
import java.nio.file.spi.FileSystemProvider;
import java.util.Set;

/**
 * Just overrides close() to be a no-op in order for it to not be closed by e.g. an in memory sftp server when it is shut down so it can be examined afterwards.
 * The method reallyClose is provided in place of close().
 * */
public class InspectableTestFileSystem extends FileSystem {
	
    final FileSystem fileSystem;

    public InspectableTestFileSystem(
        FileSystem fileSystem
    ) {
        this.fileSystem = fileSystem;
    }

    @Override
    public FileSystemProvider provider() {
        return fileSystem.provider();
    }

    @Override
    public void close(
    ){
        //will not be closed
    }

    public void reallyClose() throws IOException {
    	
    	if(fileSystem != null) {
    		fileSystem.close();
    	}
    }

    @Override
    public boolean isOpen() {
        return fileSystem.isOpen();
    }

    @Override
    public boolean isReadOnly() {
        return fileSystem.isReadOnly();
    }

    @Override
    public String getSeparator() {
        return fileSystem.getSeparator();
    }

    @Override
    public Iterable<Path> getRootDirectories() {
        return fileSystem.getRootDirectories();
    }

    @Override
    public Iterable<FileStore> getFileStores() {
        return fileSystem.getFileStores();
    }

    @Override
    public Set<String> supportedFileAttributeViews() {
        return fileSystem.supportedFileAttributeViews();
    }

    @Override
    public Path getPath(
        String first,
        String... more
    ) {
        return fileSystem.getPath(first, more);
    }

    @Override
    public PathMatcher getPathMatcher(
        String syntaxAndPattern
    ) {
        return fileSystem.getPathMatcher(syntaxAndPattern);
    }

    @Override
    public UserPrincipalLookupService getUserPrincipalLookupService() {
        return fileSystem.getUserPrincipalLookupService();
    }

    @Override
    public WatchService newWatchService() throws IOException {
        return fileSystem.newWatchService();
    }
}
