package osotnikov.test.file;

import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.Files;
import java.nio.file.Path;

public class FileUtilitiesForTesting {

	/**
	 * Returns a string that contains paths to all the directories and all the files of fs.
	 * Meant to be used with dummy file systems.
	 * */
	public static String getStringRepresentationOfFileSystem(FileSystem fs) throws IOException {
		
		Path root = fs.getPath("/");
		StringRepresentationFileVisitor fv = new StringRepresentationFileVisitor();
		Files.walkFileTree(root, fv);
		
		return fv.getStringRepresentationOfTree();
	}
}
