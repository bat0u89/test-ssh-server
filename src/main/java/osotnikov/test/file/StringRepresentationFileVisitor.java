package osotnikov.test.file;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

public class StringRepresentationFileVisitor extends SimpleFileVisitor<Path> {

	StringBuilder sb = null;
	
	public StringRepresentationFileVisitor() {
		super();
		init();
	}

	public void init() {
		sb = new StringBuilder();
	}
	
	public String getStringRepresentationOfTree() {
		return sb.toString();
	}
	
	/**
	 * Print information about each type of file. 
	 * 
     * */
    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attr) {
        if (attr.isSymbolicLink()) {
        	sb.append(String.format("Symbolic link: %s ", file));
        } else if (attr.isRegularFile()) {
        	sb.append(String.format("Regular file: %s ", file));
        } else {
        	sb.append(String.format("Other: %s ", file));
        }
        sb.append("(" + attr.size() + "bytes)");
        return FileVisitResult.CONTINUE;
    }

    /** 
     * Print each directory visited. 
     * 
     * */
    @Override
    public FileVisitResult postVisitDirectory(Path dir, IOException exc) {
    	sb.append(String.format("Directory: %s%n", dir));
        return FileVisitResult.CONTINUE;
    }

    /** 
     * If there is some error accessing the file, let the user know.
     * If you don't override this method and an error occurs, an 
     * IOException is thrown. 
     * 
     * 
    @Override
    public FileVisitResult visitFileFailed(Path file, IOException exc) {
        System.err.println(exc);
        return FileVisitResult.CONTINUE;
    }*/
}
